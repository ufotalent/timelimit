#include "stdafx.h"
#include "change1.h"
#include "type.h"
extern System::String^ StrToSys(string data);
extern string SysToStr(System::String^ data);
extern int strtoint(string data);
extern vector<DataType> data;
extern bool erase;
extern bool success;
extern DataType returndata;
extern System::String^ inttotime(int data);
extern int timetoint(System::String^ data);
System::Void timelimit::change::change_Load(System::Object^  sender, System::EventArgs^  e)
{
	success=false;
	textBox1->Text=StrToSys(returndata.taskname);
	textBox2->Text=inttotime(returndata.limittime);
	checkBox1->Checked=returndata.limit;
}
System::Void timelimit::change::button1_Click(System::Object^  sender, System::EventArgs^  e)
{
	if (timetoint(textBox2->Text)==-1)
		return;
	returndata.taskname=SysToStr(textBox1->Text);
	returndata.limit=checkBox1->Checked;
	returndata.limittime=timetoint(textBox2->Text);
	erase=checkBox2->Checked;
	change::Close();
	success=true;
}
System::Void timelimit::change::button2_Click(System::Object^  sender, System::EventArgs^  e)
{
	change::Close();
}
System::Void timelimit::change::checkBox2_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
	if ((checkBox2->Checked))
	{
			if ((MessageBox::Show("真的要清零？","询问...",System::Windows::Forms::MessageBoxButtons::YesNo,MessageBoxIcon::Warning,MessageBoxDefaultButton::Button2))==System::Windows::Forms::DialogResult::No)
		{
			checkBox2->Checked=!(checkBox2->Checked);
		}

	}
}