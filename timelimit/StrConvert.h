#pragma once
#include "vcclr.h"
#include <stdlib.h>
#include <string>
#include <windows.h>
using namespace std;
using namespace System;
using namespace System::Runtime::InteropServices;
string SysToStr(System::String ^ data);
System::String^ StrToSys(string data);
bool same(string a,string b);
int strtoint(string data);
System::String^ inttotime(int data);
int timetoint(System::String^ data);