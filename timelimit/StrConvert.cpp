#include "stdafx.h"
#include "StrConvert.h"
string SysToStr(System::String ^ data)
{
/*	pin_ptr<const wchar_t> wch = PtrToStringChars(data);
	size_t origsize = wcslen(wch) + 1;
    const size_t newsize = 100;
    size_t convertedChars = 0;
    char nstring[newsize];
    wcstombs_s(&convertedChars, nstring, origsize, wch, _TRUNCATE);
	string a(nstring);
	return a;
*/
   string a;
   for (int i=0;i<data->Length;i++)
   {
	   char temp[2];
	   wchar_t tempc;
	   tempc=data[i];
	   WideCharToMultiByte(CP_ACP,0,&tempc,1,temp,2,0,0);
	   if (temp[0]<=1)
		   a=a+temp[0]+temp[1];
	   else
		   a=a+temp[0];
   }
   return a;
}
System::String^ StrToSys(string data)
{
	char tempstr[255];
	strcpy_s(tempstr,data.c_str());
	System::String^ temp=gcnew String(tempstr);
	return temp;
}
bool same(string a,string b)
{
	if (a.length()>b.length()) return false;
	for (unsigned int i=0;i<a.length();i++)
		if (a[i]>='A'&&a[i]<='Z')
			a[i]=a[i]-'A'+'a';
	for (unsigned int i=0;i<b.length();i++)
		if (b[i]>='A'&&b[i]<='Z')
			b[i]=b[i]-'A'+'a';
	bool s=true,t=true;
	for (unsigned int i=0;i<a.length();i++)
		if (a[i]!=b[i])
			s=false;
	for (unsigned int i=1;i<=a.length();i++)
		if (a[a.length()-i]!=b[b.length()-i])
			t=false;
	return (s||t);
}
int strtoint(string data)
{
	int res=0;
	for (unsigned int i=0;i<data.length();i++)
		res=res*10+data[i]-'0';
	return res;
}
System::String^ inttotime(int data)
{
	if (data<0) data=0;
	System::String^ result;
	int hour=data/3600;
	int min=(data%3600)/60;
	int sec=(data%3600)%60;
	result=hour.ToString();
	if (min<10)
		result+=":0"+min.ToString();
	else
		result+=":"+min.ToString();
	if (sec<10)
		result+=":0"+sec.ToString();
	else
		result+=":"+sec.ToString();
	
	return result;
}
int timetoint(System::String^ data)
{
	int first=data->IndexOf(':');
	int last=data->LastIndexOf(':');
	if ((first<0)||(first==last))
	{
		System::Windows::Forms::MessageBox::Show("时间格式错误（hh::mm::ss）");
		return -1;
	}
	int res=0;
	int temp=0;
	for (int i=0;i<first;i++)
	{
		if ((data[i]>'9')||data[i]<'0')
		{
			System::Windows::Forms::MessageBox::Show("时间格式错误（hh::mm::ss）");
			return -1;
		}
		temp=temp*10+data[i]-'0';
	}
	res+=temp*3600;
	temp=0;
	for (int i=first+1;i<last;i++)
	{
		if ((data[i]>'9')||data[i]<'0')
		{
			System::Windows::Forms::MessageBox::Show("时间格式错误（hh::mm::ss）");
			return -1;
		}
		temp=temp*10+data[i]-'0';
	}
	res+=temp*60;
	temp=0;
	for (int i=last+1;i<data->Length;i++)
	{
		if ((data[i]>'9')||data[i]<'0')
		{
			System::Windows::Forms::MessageBox::Show("时间格式错误（hh::mm::ss）");
			return -1;
		}
		temp=temp*10+data[i]-'0';
	}
	res+=temp;
	return res;
}