#include "stdafx.h"
#include "Form1.h"
#include "StrConvert.h"
#include "type.h"
#include "change1.h"
extern vector<string> commandline;
vector<DataType> data;
DataType returndata;
bool erase;
bool success;
void timelimit::Form1::load()
{

	ifstream input("data.txt");
	strstream StrStream;
	DataType tempdata;
	char temp[255];
	data.clear();
	if (!input)
	{
		input.close();
		ofstream output("data.txt");
		output.close();
		input.open("data.txt");
	}
	while (input.getline(temp,255))
	{
		tempdata.running=false;
		tempdata.taskname=temp;	
		input.getline(temp,255);
		StrStream.clear();
		StrStream<<temp;
		StrStream>>tempdata.time>>tempdata.limit>>tempdata.limittime;
		data.insert(data.begin(),tempdata);
	}	
}
void timelimit::Form1::refresh(int tab)
{
	int focus=listBox1->SelectedIndex;
	int num=0;
	listBox1->Items->Clear();
	listBox2->Items->Clear();
	listBox3->Items->Clear();
	vScrollBar1->Visible=false;
	for (vector<DataType>::iterator TempIter=data.begin();TempIter!=data.end();TempIter++)
	{
		num++;
		if (num>19)
		{
			vScrollBar1->Visible=true;
		}
		if ((num>tab*19)&&(num<=tab*19+19))
		{
			listBox1->Items->Add(StrToSys((*TempIter).taskname)+((*TempIter).running?"(Running)":""));
			listBox2->Items->Add(inttotime((*TempIter).time));
			listBox3->Items->Add(inttotime((*TempIter).limittime)+((*TempIter).limit?"(限制)":"(不限制)"));
		}

	}
	if (focus>=listBox1->Items->Count)
		listBox1->SelectedIndex=listBox1->Items->Count-1;
	else
		listBox1->SelectedIndex=focus;
}
void timelimit::Form1::write()
{
	ofstream output("data.txt");
	for (vector<DataType>::iterator TempIter=data.begin();TempIter!=data.end();TempIter++)
	{
		output<<(*TempIter).taskname<<endl;
		output<<(*TempIter).time<<' '<<(*TempIter).limit<<' '<<(*TempIter).limittime<<endl;
	}
	output.close();
}

System::Void timelimit::Form1::Form1_Load(System::Object^  sender, System::EventArgs^  e) {
	load();
	sort(data.begin(),data.end());
	refresh(0);
	write();	 
	timer1->Start();
	for (vector<string>::iterator temp=commandline.begin();temp!=commandline.end();temp++)
	if ((*temp)=="/start")
		{
			Form1::WindowState=FormWindowState::Minimized;
		}

}
System::Void timelimit::Form1::timer1_Tick(System::Object^  sender, System::EventArgs^  e) {
	if ((Form1::WindowState==FormWindowState::Minimized)&&(!(notifyIcon1->Visible)))
		createicon();
	listBox4->Items->Clear();
	array<System::Diagnostics::Process^> ^processes;
	processes=System::Diagnostics::Process::GetProcesses();
	string task[255],process[255];
	int countt=0,countp=0;
	for (vector<DataType>::iterator TempIter=data.begin();TempIter!=data.end();TempIter++)
	{
		task[++countt]=(*TempIter).taskname;
	}
	for (int i=0;i<processes->Length;i++)
	{
		try
		{
			process[++countp]=SysToStr(processes[i]->Modules[0]->FileName);					
		}
		catch (System::Exception^ temp)
		{
			process[++countp]=SysToStr(processes[i]->ProcessName);
			listBox4->Items->Add(processes[i]->ProcessName+"进程打开出错，路径名用进程名称代替");
			listBox4->Refresh();
		}
		finally
		{}

	}
	for (int i=0;i<countt;i++)
	{
		data[i].running=false;
		for (int j=0;j<countp;j++)
			if (same(task[i+1],process[j+1]))
			{
				data[i].time+=5;
				data[i].running=true;
				if (data[i].limit)
				{
					if ((data[i].limittime-data[i].time<=300)&&
						(data[i].limittime>data[i].time)&&
						((data[i].limittime-data[i].time)%60==0))
					{
						(*this).Focus();
						(*this).Activate();
						MessageBox::Show(StrToSys(process[j+1])+"将在"+((data[i].limittime-data[i].time)/60).ToString()+"分钟内关闭");
					}
					bool error=false;
					if (data[i].limittime<=data[i].time)
					{
						try
						{
							processes[j]->Modules[0]->FileName;
							processes[j]->Kill();
						}
						catch (System::Exception^ a)
						{
							error=true;
							listBox4->Items->Add(StrToSys(process[j+1])+"超出限制时间，但未能正常关闭");
						}
						finally
						{
							if (!error)
							{
								(*this).Activate();
								MessageBox::Show(StrToSys(process[j+1])+"超出限制时间，已关闭");
							}

						}
					}
				}
				break;
			}
	}
	int num=0;
//	for (vector<DataType>::iterator TempIter=data.begin();TempIter!=data.end();TempIter++)
//	{
//		if (!((*TempIter).limit)) continue;
//		if (((*TempIter).limittime-(*TempIter).time<300)&&
//			(((*TempIter).limittime-(*TempIter).time)%60==0))
//				MessageBox::Show(StrToSys((*TempIter).taskname)+"将在"+(((*TempIter).limittime-(*TempIter).time)/60).ToString()+"分钟内关闭");
//		if ((*TempIter).limittime<=(*TempIter).time)
//		{
//			array<System::Diagnostics::Process^> ^processes;
//			processes=System::Diagnostics::Process::GetProcesses();
//			for (int i=0;i<processes->Length;i++)
//			{
//				if (same(SysToStr(processes[i]->Modules[0]->FileName),(*TempIter).taskname))
//					processes[i]->Close();
 //				MessageBox::Show(processes[i]->ProcessName+"已关闭");
//			}
//		}
//	}
	for (vector<DataType>::iterator TempIter=data.begin();TempIter!=data.end();TempIter++)
		num++;
	num=num/19;
	num++;
	refresh(num*(vScrollBar1->Value/91.0-1e-6));
}
System::Void timelimit::Form1::vScrollBar1_Scroll(System::Object^  sender, System::Windows::Forms::ScrollEventArgs^  e) {
	int num=0;
	for (vector<DataType>::iterator TempIter=data.begin();TempIter!=data.end();TempIter++)
		num++;
	num=num/19;
	num++;

	refresh(num*(vScrollBar1->Value/91.0-1e-6));
}
System::Void timelimit::Form1::Form1_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
	if ((e->CloseReason)==(System::Windows::Forms::CloseReason::UserClosing))
	{
		if ((MessageBox::Show("确定关闭？","询问...",System::Windows::Forms::MessageBoxButtons::YesNo,MessageBoxIcon::Warning,MessageBoxDefaultButton::Button2))==System::Windows::Forms::DialogResult::No)
		{
			e->Cancel=true;
			return;
		}
	}
	write();
	notifyIcon1->Visible=false;
}
System::Void timelimit::Form1::Form1_MouseClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) 
{
	/*
	typedef void (*USE)(HWND hWnd,unsigned int Msg,WPARAM wParam,LPARAM lParam);
	typedef HWND (*USE_)(LPCWSTR lpClassName,LPCWSTR lpWindowName);
	USE sendmessage;
	USE_ findwindow;
	HINSTANCE hDLL;
	hDLL=LoadLibrary(L"sendmessagedll.dll");
	if (hDLL!=NULL)
	{
	findwindow=(USE_)GetProcAddress(hDLL,"findwindow");
	sendmessage=(USE)GetProcAddress(hDLL,"sendmessage");
	if (!sendmessage)
	MessageBox::Show("fail");
	}
	else
	MessageBox::Show("fail");
	*/
	//		HWND temphandle=findwindow(NULL,L"change");
}
void timelimit::Form1::deleteitem()
{
	if ((MessageBox::Show("真的要删除？","询问...",System::Windows::Forms::MessageBoxButtons::YesNo,MessageBoxIcon::Warning,MessageBoxDefaultButton::Button2))==System::Windows::Forms::DialogResult::No)
	{
		return;
	}
	if (listBox1->SelectedIndex<0) 
		return;
	for (vector<DataType>::iterator tempiter=data.begin();tempiter!=data.end();tempiter++)
	{
		int a=((*tempiter).taskname+((*tempiter).running?"(Running)":"")).compare(SysToStr(listBox1->Items[listBox1->SelectedIndex]->ToString()));
		if (a==0)
		{
			data.erase(tempiter);
			int num=0;
			for (vector<DataType>::iterator TempIter=data.begin();TempIter!=data.end();TempIter++)
				num++;
			num=num/19;
			num++;
			refresh(num*(vScrollBar1->Value/91.0-1e-6));
			return;
		}
	}
}
void timelimit::Form1::modifyitem()
{
	if (listBox1->SelectedIndex<0) 
		return;
	for (vector<DataType>::iterator tempiter=data.begin();tempiter!=data.end();tempiter++)
	{
		int a=((*tempiter).taskname+((*tempiter).running?"(Running)":"")).compare(SysToStr(listBox1->Items[listBox1->SelectedIndex]->ToString()));
		if (a==0)
		{
			returndata=(*tempiter);
			(gcnew change)->ShowDialog();
			if (!success) continue;
			(*tempiter).limit=returndata.limit;
			(*tempiter).taskname=returndata.taskname;
			(*tempiter).limittime=returndata.limittime;
			if (erase)
				(*tempiter).time=0;
			int num=0;
			for (vector<DataType>::iterator TempIter=data.begin();TempIter!=data.end();TempIter++)
				num++;
			num=num/19;
			num++;
			refresh(num*(vScrollBar1->Value/91.0-1e-6));
			return;
		}
	}	
}
void timelimit::Form1::insertitem()
{
	DataType tempdata;
	vector<DataType>::iterator tempiter;
	returndata.time=0;
	(gcnew change)->ShowDialog();
	if (!success) return;
	data.insert(data.end(),returndata);
	int num=0;
	for (vector<DataType>::iterator TempIter=data.begin();TempIter!=data.end();TempIter++)
		num++;
	num=num/19;
	num++;
	refresh(num*(vScrollBar1->Value/91.0-1e-6));
}
void timelimit::Form1::createicon()
{
	notifyIcon1->Visible=true;
}
void timelimit::Form1::deleteicon()
{
	notifyIcon1->Visible=false;
}
System::Void timelimit::Form1::listBox1_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e)
{
	switch (e->KeyValue)
	{
	case VK_DELETE:
		deleteitem();
		break;
	case 13:
		modifyitem();
		break;
	}
}
System::Void timelimit::Form1::button1_Click(System::Object^  sender, System::EventArgs^  e)
{
	modifyitem();
}
System::Void timelimit::Form1::button2_Click(System::Object^  sender, System::EventArgs^  e)
{
	deleteitem();
}
System::Void timelimit::Form1::button3_Click(System::Object^  sender, System::EventArgs^  e)
{
	insertitem();
}
System::Void timelimit::Form1::listBox1_DoubleClick(System::Object^  sender, System::EventArgs^  e)
{
	if (listBox1->SelectedIndex>=0)
		modifyitem();
}
System::Void timelimit::Form1::notifyIcon1_MouseDoubleClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
	Form1::WindowState=FormWindowState::Normal;
	deleteicon();
	Form1::ShowInTaskbar=true;
	Form1::Show();
	Form1::Activate();
}
System::Void timelimit::Form1::Form1_SizeChanged(System::Object^  sender, System::EventArgs^  e)
{
	if (Form1::WindowState==FormWindowState::Minimized)
	{
		Form1::ShowInTaskbar=false;
		Form1::Hide();
		createicon();
	}
}
System::Void timelimit::Form1::notifyIcon1_MouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
	notifyIcon1->Text="";
	for (vector<DataType>::iterator temp=data.begin();temp!=data.end();temp++)
	{
		if ((*temp).running&&(*temp).limit)
		{
			try
			{
				notifyIcon1->Text+=StrToSys((*temp).taskname)+"剩余"+inttotime((*temp).limittime-(*temp).time)+"\n";
			}
			catch	(System::Exception^ temp)
			{
			}
		}
	}
}

System::Void timelimit::Form1::aToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
	Form1::WindowState=FormWindowState::Normal;
	deleteicon();
	Form1::ShowInTaskbar=true;
	Form1::Show();
	Form1::Activate();
}
System::Void timelimit::Form1::退出ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
	Form1::Close();
}